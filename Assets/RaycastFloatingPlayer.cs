﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastFloatingPlayer : MonoBehaviour
{
    private float lenghtOfRayCast = 100f;

    // Update is called once per frame
    void Update()
    {
        Ray ray = new Ray(transform.position, -transform.up);
        RaycastHit hitInfo;

        if(!Physics.Raycast(ray,out hitInfo, lenghtOfRayCast))
        {
            Debug.DrawLine(ray.origin, ray.origin + ray.direction * lenghtOfRayCast, Color.red);
        }
    }
}
